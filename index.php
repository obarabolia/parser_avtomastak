<?php

set_time_limit(0);
require "phpQuery/phpQuery.php";

//parseMastak();
parseMasterInstrument();

function parseMastak()
{
    $base_url = 'http://www.mactak.ru';
    $pages_url = ['/store/mactak/?start=', '/store/kingtony/?start=', '/store/mightyseven/?start='];
    echo "<br>";
    echo "<b>$base_url</b>";
    echo "<br>";
    echo "<br>";

    $dom = new DomDocument('1.0');
    $imp = new DOMImplementation;
    $dtd = $imp->createDocumentType('yml_catalog', '', 'shops.dtd');
    $dom = $imp->createDocument("", "", $dtd);
    $dom->encoding = 'UTF-8';
    $yml_catalog = $dom->appendChild($dom->createElement('yml_catalog'));
    $yml_catalog->setAttribute('date', date('Y-m-d h:i'));

        $shop = $yml_catalog->appendChild($dom->createElement('shop'));
        $shop_name = $shop->appendChild($dom->createElement('name'));
        $shop_name->appendChild($dom->createTextNode('www.mactak.ru'));

        $shop_company = $shop->appendChild($dom->createElement('company'));
        $shop_company->appendChild($dom->createTextNode('mactak'));

        $shop_url = $shop->appendChild($dom->createElement('url'));
        $shop_url->appendChild($dom->createTextNode('http://www.mactak.ru'));

        $currencies = $shop->appendChild($dom->createElement('currencies'));
        $currency = $currencies->appendChild($dom->createElement('currency'));
        $currency->setAttribute('id', 'RUR');
        $currency->setAttribute('rate', '1');

        include("category_tree.php");

        $offers = $shop->appendChild($dom->createElement('offers'));

    $category_mastak = [];
    foreach($pages_url as $page_url) {
        echo "<b>$base_url.$page_url</b>";
        echo "<br>";
        $results_page = file_get_contents($base_url . $page_url);
        $products_url = phpQuery::newDocument($results_page);

        $page_end = $products_url->find('ul > li.pagination-end > a')->attr('href');
        $end = file_get_contents($base_url . $page_end);
        $end_num = phpQuery::newDocument($end);
        $end_page_num = $end_num->find('ul > li:nth-child(11) > a')->attr('href');
        preg_match('/\d+/', $end_page_num, $max_num);
        $max_num[0] += 15;

        $arr_pages_url = [];

        for ($i = 0; $i <= $max_num[0]; $i += 15) {
            $url = "$page_url$i";
            array_unshift($arr_pages_url, $url);
        }

        /*------------pages--------------------*/

        foreach ($arr_pages_url as $page) {
            echo "<b>$page</b>";
            echo "<br>";
            //sleep(1);
            $results_page = file_get_contents($base_url . $page);
            $products_url = phpQuery::newDocument($results_page);
            $elements = $products_url->find('tr > td.block_product > div > div:nth-child(4) > a');


            /*------------product_pages--------------------*/

            $arr_hrefs = [];
            foreach ($elements as $element) {
                $href = pq($element)->attr('href');
                array_unshift($arr_hrefs, $href);
            }

            foreach ($arr_hrefs as $arr_href) {
                echo $arr_href;
                echo "<br>";
                //sleep(1);
                $product = file_get_contents($base_url . $arr_href);
                $product_url = phpQuery::newDocument($product);

                $category_path = explode("\n", $product_url->find('div.breadcrumbs > a')->text());
                array_pop($category_path); array_shift($category_path);

                $category_path = implode("|", $category_path);
                array_push($category_mastak, $category_path);

                $name = $product_url->find("div.breadcrumbs > span")->text();

                $articul = $product_url->find("#product_code")->text();

                $specific_style = $product_url->find("div.jshop_prod_description")->html();
                $specific_tag_img = preg_replace("/style=\".+\"/", "", $specific_style);

                $specific_img = preg_replace("/\/images\/instrykii\/icon_pdf.png/", "http://avto-mactak.ru/image/data/pdfdownload.png", $specific_tag_img);
                preg_match("/\/images.+\.pdf/", $specific_img, $specific_a_march);

                preg_match("/((?<=instrykii\/).+)|((?<=pdf\/).+)/", $specific_a_march[0], $instruction_name);

                if(isset($instruction_name[0]))
                {
                    $instructions = "$articul.pdf";
                    $path_public = $_SERVER['DOCUMENT_ROOT']."/image/data/instructions/$instructions";
                    $url = "http://www.mactak.ru$specific_a_march[0]";
                    file_put_contents($path_public, file_get_contents($url));
                }

                $specific = preg_replace("/\/images.+\.pdf/", "http://avto-mactak.ru/image/data/instructions/$instructions", $specific_img);

                $imgs = [];
                $mains_img_url = $product_url->find('a.lightbox > img');
                foreach ($mains_img_url as $main_img_url) {
                    $img_url = pq($main_img_url)->attr('src');
                    array_push($imgs, $img_url);
                }

                $price_full = $product_url->find('span#block_price')->text();
                preg_match_all("/\d+/", $price_full, $price_all);
                $price = implode('', $price_all[0]);

                $manufacturer = $product_url->find('div.manufacturer_name > span')->text();

                /*------------------XML---------------------*/

                $product = $offers->appendChild($dom->createElement('offer'));
                $product->setAttribute('available', 'true');

                $product_link = $product->appendChild($dom->createElement('url'));
                $product_link->appendChild($dom->createTextNode($base_url.$arr_href));

                $price_xml = $product->appendChild($dom->createElement('price'));
                $price_xml->appendChild($dom->createTextNode($price));

                $currencies_id = $product->appendChild($dom->createElement('currencyId'));
                $currencies_id->appendChild($dom->createTextNode("RUR"));

                include("category.php");
                foreach ($all_category["mastak"] as $key => $value)
                {
                    if($key == $category_path)
                    {
                        $category_id = $product->appendChild($dom->createElement('categoryId'));
                        $category_id->appendChild($dom->createTextNode($value));
                    }
                }

                foreach ($imgs as $img) {
                    if($main_img_url)
                    {
                        $img_xml = $product->appendChild($dom->createElement('picture'));
                        $img_xml->appendChild($dom->createTextNode($img));
                    }
                }

                $name_product = $product->appendChild($dom->createElement('name'));
                $name_product->appendChild($dom->createTextNode($name));

                $manufacturer_xml = $product->appendChild($dom->createElement('vendor'));
                $manufacturer_xml->appendChild($dom->createTextNode($manufacturer));

                $articul_xml = $product->appendChild($dom->createElement('vendorCode'));
                $articul_xml->appendChild($dom->createTextNode(iconv("Windows-1251", "UTF-8", $articul)));

                $specific_xml = $product->appendChild($dom->createElement('description'));
                $specific_xml->appendChild($dom->createTextNode($specific));
            }
            break;
        }
    }
/*echo "<pre>";
print_r($category_mastak);
echo "</pre>";
$unique_category_mastak = array_unique($category_mastak);
echo "<pre>";
print_r($unique_category_mastak);
echo "</pre>";*/
$dom->formatOutput = true;
$data_xml = $dom->saveXML();
echo $dom->save('mastak.xml');
}

function parseMasterInstrument()
{
    $base_url = 'http://www.master-instrument.ru';
    $page_url = '/vendors/id11938/?PAGEN_2=';

    $results_page = file_get_contents("http://www.master-instrument.ru/vendors/id11938/");

    $products_url = phpQuery::newDocument($results_page);
    $pages_count = $products_url->find('div.page-navigation > a:nth-child(7)')->text();
    $pages_url = $products_url->find('div.page-navigation > a');

    $arr_pages_url = [];
    for($i = 0; $i <= $pages_count; $i++)
    {
        $url = "$page_url$i";
        array_unshift($arr_pages_url, $url);
    }
    array_pop($arr_pages_url);

    $dom = new DomDocument('1.0');
    $imp = new DOMImplementation;
    $dtd = $imp->createDocumentType('yml_catalog', '', 'shops.dtd');
    $dom = $imp->createDocument("", "", $dtd);
    $dom->encoding = 'UTF-8';
    $yml_catalog = $dom->appendChild($dom->createElement('yml_catalog'));
    $yml_catalog->setAttribute('date', date('Y-m-d h:i'));

        $shop = $yml_catalog->appendChild($dom->createElement('shop'));
        $shop_name = $shop->appendChild($dom->createElement('name'));
        $shop_name->appendChild($dom->createTextNode('www.master-instrument.ru'));

        $shop_company = $shop->appendChild($dom->createElement('company'));
        $shop_company->appendChild($dom->createTextNode('master-instrument'));

        $shop_url = $shop->appendChild($dom->createElement('url'));
        $shop_url->appendChild($dom->createTextNode('http://www.master-instrument.ru'));

        $currencies = $shop->appendChild($dom->createElement('currencies'));
            $currency = $currencies->appendChild($dom->createElement('currency'));
            $currency->setAttribute('id', 'RUR');
            $currency->setAttribute('rate', '1');

        include("category_tree.php");

        $offers = $shop->appendChild($dom->createElement('offers'));

    /*------------pages--------------------*/
    echo "<br>";
    echo "<b>$base_url</b>";
    echo "<br>";
    $category_master_intrument = [];
    foreach($arr_pages_url as $page)
    {
        //sleep(1);
        $results_page = file_get_contents($base_url . $page);
        $products_url = phpQuery::newDocument($results_page);
        $elements = $products_url->find('div.product-info-block > div.bordered.product_name > a');
        echo "<b>$page</b>";
        echo "<br>";

        /*------------product_pages--------------------*/

        $arr_hrefs = [];
        foreach($elements as $element){
            $href = pq($element)->attr('href');
            array_unshift($arr_hrefs, $href);
            echo $href;
            echo "<br>";
        }

        foreach ($arr_hrefs as $arr_href)
        {
            //sleep(1);
            $product = file_get_contents($base_url . $arr_href);

            $product_url = phpQuery::newDocument($product);

            $category_path_dirt = explode("/", trim($product_url->find('.bx-breadcrumb-item')->text()));
            array_pop($category_path_dirt); array_shift($category_path_dirt);
            $category_path = [];
            foreach($category_path_dirt as $cat_path)
            {
                array_unshift($category_path, trim($cat_path));
            }

            $category_path = implode("|", $category_path);
            array_push($category_master_intrument, $category_path);

            $name = $product_url->find("div.bx_item_container > div.bx_rt > h1 > meta")->attr('content');

            $articul = $product_url->find(".item_info_section")->html();
            $pattern = '<!--<div class=\"articul-block\">.{8}(.+?)<\/div>-->';
            $matches = 0;
            preg_match($pattern, $articul, $matches);
            array_shift($matches);

            $specific_tag = $product_url->find("ul.specific-ul > div")->html();
            $specific = iconv("Windows-1251", "UTF-8", $specific_tag);

            $perfomance_name = explode("\n", $product_url->find("div.performance > div > div.line > .name")->text());
            $perfomance_value = explode("\n", $product_url->find("div.performance > div > div.line > .value")->text());

            $imgs = [];
            $mains_img_url = $product_url->find('a.fancybox');

            foreach($mains_img_url as $main_img_url)
            {
                $image = pq($main_img_url)->attr('href');
                if($image)
                    array_push($imgs, $image);
            }

            $price = $product_url->find('div.item_current_price > meta')->attr('content');
            //$manufacturer = $product_url->find('div.cell.description > div.vendor-block > a')->text();

            /*------------------XML---------------------*/

            $product = $offers->appendChild($dom->createElement('offer'));
            $product->setAttribute('available', 'true');


            $product_link = $product->appendChild($dom->createElement('url'));
            $product_link->appendChild($dom->createTextNode($base_url.$arr_href));

            $price_xml = $product->appendChild($dom->createElement('price'));
            $price_xml->appendChild($dom->createTextNode($price));

            $currencies_id = $product->appendChild($dom->createElement('currencyId'));
            $currencies_id->appendChild($dom->createTextNode("RUR"));

            //include("category_id.php");

            foreach($imgs as $img)
            {
                if($image)
                {
                    $img_xml = $product->appendChild($dom->createElement('picture'));
                    $img_xml->appendChild($dom->createTextNode($base_url.$img));
                }
            }

            $name_product = $product->appendChild($dom->createElement('name'));
            $name_product->appendChild($dom->createTextNode($name));

            $manufacturer_xml = $product->appendChild($dom->createElement('vendor'));
            $manufacturer_xml->appendChild($dom->createTextNode('NORDBERG'));

            $articul_xml = $product->appendChild($dom->createElement('vendorCode'));
            $articul_xml->appendChild($dom->createTextNode(iconv("Windows-1251", "UTF-8", $matches[0])));

            $specific_xml = $product->appendChild($dom->createElement('description'));
            $specific_xml->appendChild($dom->createTextNode($specific));

            for($i = 0; $i <= count($perfomance_name) - 2; $i++)
            {
                $perfomence_xml = $product->appendChild($dom->createElement('param'));
                $perfomence_xml->setAttribute('name', "$perfomance_name[$i]");
                $perfomence_xml->appendChild($dom->createTextNode($perfomance_value[$i]));
            }
        }
    }
echo "<pre>";
print_r($category_master_intrument);
echo "</pre>";
$unique_category_mastak = array_unique($category_master_intrument);
echo "<pre>";
print_r($unique_category_mastak);
echo "</pre>";
$dom->formatOutput = true;
$data_xml = $dom->saveXML();
echo $dom->save('master_instrument.xml');
echo "<br>";
}

?>