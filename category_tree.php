<?php
$categories = $shop->appendChild($dom->createElement('categories'));

	$category1 = $categories->appendChild($dom->createElement('category'));
	$category1->setAttribute('id', '1');
	$category1->appendChild($dom->createTextNode('Подъемное оборудование, гидравлические прессы'));

		$category11 = $categories->appendChild($dom->createElement('category'));
        $category11->setAttribute('id', '2');
        $category11->setAttribute('parentId', '1');
        $category11->appendChild($dom->createTextNode('Автомобильные подъемники'));

        $category01 = $categories->appendChild($dom->createElement('category'));
        $category01->setAttribute('id', '2999');
        $category01->setAttribute('parentId', '1');
        $category01->appendChild($dom->createTextNode('Гидравлические краны'));

        $category12 = $categories->appendChild($dom->createElement('category'));
        $category12->setAttribute('id', '3');
        $category12->setAttribute('parentId', '1');
        $category12->appendChild($dom->createTextNode('Гидравлические прессы'));

	        $category121 = $categories->appendChild($dom->createElement('category'));
	        $category121->setAttribute('id', '4');
	        $category121->setAttribute('parentId', '3');
	        $category121->appendChild($dom->createTextNode('Аксессуары к гидравлическим прессам'));

        $category13 = $categories->appendChild($dom->createElement('category'));
        $category13->setAttribute('id', '5');
        $category13->setAttribute('parentId', '1');
        $category13->appendChild($dom->createTextNode('Двухстоечные подъемники'));

	        $category131 = $categories->appendChild($dom->createElement('category'));
	        $category131->setAttribute('id', '6');
	        $category131->setAttribute('parentId', '5');
	        $category131->appendChild($dom->createTextNode('Запчасти и аксессуары для двухстоечных подъемников'));

		$category14 = $categories->appendChild($dom->createElement('category'));
        $category14->setAttribute('id', '7');
        $category14->setAttribute('parentId', '1');
        $category14->appendChild($dom->createTextNode('Домкраты бутылочные гидравлические'));

        $category15 = $categories->appendChild($dom->createElement('category'));
        $category15->setAttribute('id', '8');
        $category15->setAttribute('parentId', '1');
        $category15->appendChild($dom->createTextNode('Домкраты пневматические'));

        $category16 = $categories->appendChild($dom->createElement('category'));
        $category16->setAttribute('id', '9');
        $category16->setAttribute('parentId', '1');
        $category16->appendChild($dom->createTextNode('Домкраты пневмогидравлические'));

            $category161 = $categories->appendChild($dom->createElement('category'));
	        $category161->setAttribute('id', '10');
	        $category161->setAttribute('parentId', '9');
	        $category161->appendChild($dom->createTextNode('Запчасти и аксессуары для пневмогидравлических домкратов'));

		$category17 = $categories->appendChild($dom->createElement('category'));
        $category17->setAttribute('id', '11');
        $category17->setAttribute('parentId', '1');
        $category17->appendChild($dom->createTextNode('Домкраты подкатные'));

			$category171 = $categories->appendChild($dom->createElement('category'));
	        $category171->setAttribute('id', '12');
	        $category171->setAttribute('parentId', '11');
	        $category171->appendChild($dom->createTextNode('Запчасти для подкатных домкратов'));

			$category172 = $categories->appendChild($dom->createElement('category'));
	        $category172->setAttribute('id', '13');
	        $category172->setAttribute('parentId', '11');
	        $category172->appendChild($dom->createTextNode('Насадки на подкатные домкраты'));

        $category18 = $categories->appendChild($dom->createElement('category'));
        $category18->setAttribute('id', '14');
        $category18->setAttribute('parentId', '1');
        $category18->appendChild($dom->createTextNode('Ножничные подъемники'));

	        $category181 = $categories->appendChild($dom->createElement('category'));
	        $category181->setAttribute('id', '15');
	        $category181->setAttribute('parentId', '14');
	        $category181->appendChild($dom->createTextNode('Запчасти и аксессуары для ножничных подъемников'));

	    $category19 = $categories->appendChild($dom->createElement('category'));
        $category19->setAttribute('id', '16');
        $category19->setAttribute('parentId', '1');
        $category19->appendChild($dom->createTextNode('Оснастка и приспособления для гидравлического оборудования'));

        $category19a1 = $categories->appendChild($dom->createElement('category'));
        $category19a1->setAttribute('id', '17');
        $category19a1->setAttribute('parentId', '1');
        $category19a1->appendChild($dom->createTextNode('Парковочные системы'));

		$category19a2 = $categories->appendChild($dom->createElement('category'));
        $category19a2->setAttribute('id', '18');
        $category19a2->setAttribute('parentId', '1');
        $category19a2->appendChild($dom->createTextNode('Подставки под машину'));

        $category19a3 = $categories->appendChild($dom->createElement('category'));
        $category19a3->setAttribute('id', '19');
        $category19a3->setAttribute('parentId', '1');
        $category19a3->appendChild($dom->createTextNode('Подъемники для мотоциклов'));

        $category19a4 = $categories->appendChild($dom->createElement('category'));
        $category19a4->setAttribute('id', '20');
        $category19a4->setAttribute('parentId', '1');
        $category19a4->appendChild($dom->createTextNode('Подъемные столы'));

        $category19a5 = $categories->appendChild($dom->createElement('category'));
        $category19a5->setAttribute('id', '21');
        $category19a5->setAttribute('parentId', '1');
        $category19a5->appendChild($dom->createTextNode('Системы парковки'));

        $category19a6 = $categories->appendChild($dom->createElement('category'));
        $category19a6->setAttribute('id', '22');
        $category19a6->setAttribute('parentId', '1');
        $category19a6->appendChild($dom->createTextNode('Стойки механические'));

        $category19a6a = $categories->appendChild($dom->createElement('category'));
        $category19a6a->setAttribute('id', '2211');
        $category19a6a->setAttribute('parentId', '1');
        $category19a6a->appendChild($dom->createTextNode('Стойки гидравлические'));

        $category19a7 = $categories->appendChild($dom->createElement('category'));
        $category19a7->setAttribute('id', '23');
        $category19a7->setAttribute('parentId', '1');
        $category19a7->appendChild($dom->createTextNode('Траверсы'));

        $category19a8 = $categories->appendChild($dom->createElement('category'));
        $category19a8->setAttribute('id', '24');
        $category19a8->setAttribute('parentId', '1');
        $category19a8->appendChild($dom->createTextNode('Трансмиссионные стойки'));

	        $category19a81 = $categories->appendChild($dom->createElement('category'));
	        $category19a81->setAttribute('id', '25');
	        $category19a81->setAttribute('parentId', '24');
	        $category19a81->appendChild($dom->createTextNode('Насадки для трансмиссионных стоек'));

        $category19a9 = $categories->appendChild($dom->createElement('category'));
        $category19a9->setAttribute('id', '26');
        $category19a9->setAttribute('parentId', '1');
        $category19a9->appendChild($dom->createTextNode('Четырехстоечные подъемники'));

	        $category19a91 = $categories->appendChild($dom->createElement('category'));
	        $category19a91->setAttribute('id', '27');
	        $category19a91->setAttribute('parentId', '26');
	        $category19a91->appendChild($dom->createTextNode('Запчасти и аксессуары для четырехстоечных подъемников'));

	$category2 = $categories->appendChild($dom->createElement('category'));
	$category2->setAttribute('id', '28');
	$category2->appendChild($dom->createTextNode('Окрасочное оборудование'));

		$category21 = $categories->appendChild($dom->createElement('category'));
        $category21->setAttribute('id', '29');
        $category21->setAttribute('parentId', '28');
        $category21->appendChild($dom->createTextNode('Окрасочные камеры'));

	        $category211 = $categories->appendChild($dom->createElement('category'));
	        $category211->setAttribute('id', '30');
	        $category211->setAttribute('parentId', '29');
	        $category211->appendChild($dom->createTextNode('Аксессуары для окрасочных камер'));

	    $category22 = $categories->appendChild($dom->createElement('category'));
        $category22->setAttribute('id', '31');
        $category22->setAttribute('parentId', '28');
        $category22->appendChild($dom->createTextNode('Посты подготовки к окраске'));

        	$category221 = $categories->appendChild($dom->createElement('category'));
	        $category221->setAttribute('id', '32');
	        $category221->setAttribute('parentId', '31');
	        $category221->appendChild($dom->createTextNode('Аксессуары для постов подготовки к окраске'));

	    $category23 = $categories->appendChild($dom->createElement('category'));
        $category23->setAttribute('id', '33');
        $category23->setAttribute('parentId', '28');
        $category23->appendChild($dom->createTextNode('Системы подготовки сжатого воздуха'));

    	$category24 = $categories->appendChild($dom->createElement('category'));
        $category24->setAttribute('id', '34');
        $category24->setAttribute('parentId', '28');
        $category24->appendChild($dom->createTextNode('Сушки инфракрасные'));

        	$category241 = $categories->appendChild($dom->createElement('category'));
	        $category241->setAttribute('id', '35');
	        $category241->setAttribute('parentId', '34');
	        $category241->appendChild($dom->createTextNode('Лампы инфракрасные для сушек'));

	    $category25 = $categories->appendChild($dom->createElement('category'));
        $category25->setAttribute('id', '36');
        $category25->setAttribute('parentId', '28');
        $category25->appendChild($dom->createTextNode('Краскопульты, аэрографы'));

        $category26 = $categories->appendChild($dom->createElement('category'));
        $category26->setAttribute('id', '37111');
        $category26->setAttribute('parentId', '28');
        $category26->appendChild($dom->createTextNode('Вспомогательное оборудование'));

    $category3 = $categories->appendChild($dom->createElement('category'));
    $category3->setAttribute('id', '37');
    $category3->appendChild($dom->createTextNode('Шиномонтажное оборудование'));

    	$category31 = $categories->appendChild($dom->createElement('category'));
        $category31->setAttribute('id', '38');
        $category31->setAttribute('parentId', '37');
        $category31->appendChild($dom->createTextNode('Шиномонтажные станки'));

	        $category311 = $categories->appendChild($dom->createElement('category'));
	        $category311->setAttribute('id', '39');
	        $category311->setAttribute('parentId', '38');
	        $category311->appendChild($dom->createTextNode('Запчасти и аксессуары для шиномонтажных станков'));

        $category32 = $categories->appendChild($dom->createElement('category'));
        $category32->setAttribute('id', '40');
        $category32->setAttribute('parentId', '37');
        $category32->appendChild($dom->createTextNode('Балансировочные станки'));

	        $category321 = $categories->appendChild($dom->createElement('category'));
	        $category321->setAttribute('id', '41');
	        $category321->setAttribute('parentId', '40');
	        $category321->appendChild($dom->createTextNode('Запчасти и аксессуары для балансировочных станков'));

        $category33 = $categories->appendChild($dom->createElement('category'));
        $category33->setAttribute('id', '42');
        $category33->setAttribute('parentId', '37');
        $category33->appendChild($dom->createTextNode('Дископравы'));

	        $category331 = $categories->appendChild($dom->createElement('category'));
	        $category331->setAttribute('id', '43');
	        $category331->setAttribute('parentId', '42');
	        $category331->appendChild($dom->createTextNode('Запчасти и аксессуары для дископравов'));

        $category34 = $categories->appendChild($dom->createElement('category'));
        $category34->setAttribute('id', '44');
        $category34->setAttribute('parentId', '37');
        $category34->appendChild($dom->createTextNode('Вулканизаторы'));

		    $category341 = $categories->appendChild($dom->createElement('category'));
		    $category341->setAttribute('id', '45');
		    $category341->setAttribute('parentId', '44');
		    $category341->appendChild($dom->createTextNode('Аксессуары для вулканизатора'));

	    $category35 = $categories->appendChild($dom->createElement('category'));
	    $category35->setAttribute('id', '46');
	    $category35->setAttribute('parentId', '37');
	    $category35->appendChild($dom->createTextNode('Гайковерты'));

		$category36 = $categories->appendChild($dom->createElement('category'));
	    $category36->setAttribute('id', '47');
	    $category36->setAttribute('parentId', '37');
	    $category36->appendChild($dom->createTextNode('Борторасширители'));

	    $category37 = $categories->appendChild($dom->createElement('category'));
	    $category37->setAttribute('id', '48');
	    $category37->setAttribute('parentId', '37');
	    $category37->appendChild($dom->createTextNode('Бортоотжиматели'));

	    $category38 = $categories->appendChild($dom->createElement('category'));
	    $category38->setAttribute('id', '49');
	    $category38->setAttribute('parentId', '37');
	    $category38->appendChild($dom->createTextNode('Ванны для проверки колес'));

	    $category39 = $categories->appendChild($dom->createElement('category'));
	    $category39->setAttribute('id', '50');
	    $category39->setAttribute('parentId', '37');
	    $category39->appendChild($dom->createTextNode('Генераторы азота'));

	    $category39a1 = $categories->appendChild($dom->createElement('category'));
	    $category39a1->setAttribute('id', '51');
	    $category39a1->setAttribute('parentId', '37');
	    $category39a1->appendChild($dom->createTextNode('Пистолеты для подкачки шин'));

	    $category39a2 = $categories->appendChild($dom->createElement('category'));
	    $category39a2->setAttribute('id', '52');
	    $category39a2->setAttribute('parentId', '37');
	    $category39a2->appendChild($dom->createTextNode('Резервуары для подкачки колес (бустер)'));

	    $category39a3 = $categories->appendChild($dom->createElement('category'));
	    $category39a3->setAttribute('id', '53');
	    $category39a3->setAttribute('parentId', '37');
	    $category39a3->appendChild($dom->createTextNode('Расходные материалы для шиномонтажа'));

		    $category39a31 = $categories->appendChild($dom->createElement('category'));
		    $category39a31->setAttribute('id', '54');
		    $category39a31->setAttribute('parentId', '53');
		    $category39a31->appendChild($dom->createTextNode('Балансировочные грузики'));

		    $category39a32 = $categories->appendChild($dom->createElement('category'));
		    $category39a32->setAttribute('id', '55');
		    $category39a32->setAttribute('parentId', '53');
		    $category39a32->appendChild($dom->createTextNode('Паста монтажная'));

		    $category39a33 = $categories->appendChild($dom->createElement('category'));
		    $category39a33->setAttribute('id', '56');
		    $category39a33->setAttribute('parentId', '53');
		    $category39a33->appendChild($dom->createTextNode('Ручной инструмент'));

	$category4 = $categories->appendChild($dom->createElement('category'));
    $category4->setAttribute('id', '57');
    $category4->appendChild($dom->createTextNode('Гаражное оборудование'));

	    $category41 = $categories->appendChild($dom->createElement('category'));
	    $category41->setAttribute('id', '58');
	    $category41->setAttribute('parentId', '57');
	    $category41->appendChild($dom->createTextNode('Восстановление шаровых опор'));
	    
	    $category42 = $categories->appendChild($dom->createElement('category'));
	    $category42->setAttribute('id', '59');
	    $category42->setAttribute('parentId', '57');
	    $category42->appendChild($dom->createTextNode('Кантователи и траверсы для двигателя'));

	    $category43 = $categories->appendChild($dom->createElement('category'));
	    $category43->setAttribute('id', '60');
	    $category43->setAttribute('parentId', '57');
	    $category43->appendChild($dom->createTextNode('Лампы переноски, фонари'));

	    $category44 = $categories->appendChild($dom->createElement('category'));
	    $category44->setAttribute('id', '61');
	    $category44->setAttribute('parentId', '57');
	    $category44->appendChild($dom->createTextNode('Оборудование для замены масла'));

		    $category441 = $categories->appendChild($dom->createElement('category'));
		    $category441->setAttribute('id', '62');
		    $category441->setAttribute('parentId', '61');
		    $category441->appendChild($dom->createTextNode('Запасные части'));

		    $category442 = $categories->appendChild($dom->createElement('category'));
		    $category442->setAttribute('id', '63');
		    $category442->setAttribute('parentId', '61');
		    $category442->appendChild($dom->createTextNode('Оборудование для раздачи масла'));

		    $category443 = $categories->appendChild($dom->createElement('category'));
		    $category443->setAttribute('id', '64');
		    $category443->setAttribute('parentId', '61');
		    $category443->appendChild($dom->createTextNode('Оборудование для слива масла'));

		$category45 = $categories->appendChild($dom->createElement('category'));
	    $category45->setAttribute('id', '65');
	    $category45->setAttribute('parentId', '64');
	    $category45->appendChild($dom->createTextNode('Оборудование для замены технических жидкостей'));

	    $category46 = $categories->appendChild($dom->createElement('category'));
	    $category46->setAttribute('id', '66');
	    $category46->setAttribute('parentId', '64');
	    $category46->appendChild($dom->createTextNode('Столы для работы со стеклами'));

	    $category47 = $categories->appendChild($dom->createElement('category'));
	    $category47->setAttribute('id', '67');
	    $category47->setAttribute('parentId', '64');
	    $category47->appendChild($dom->createTextNode('Тали'));

	    $category48 = $categories->appendChild($dom->createElement('category'));
	    $category48->setAttribute('id', '68');
	    $category48->setAttribute('parentId', '64');
	    $category48->appendChild($dom->createTextNode('Тиски'));

	    $category49 = $categories->appendChild($dom->createElement('category'));
	    $category49->setAttribute('id', '69');
	    $category49->setAttribute('parentId', '64');
	    $category49->appendChild($dom->createTextNode('Удаление выхлопных газов'));

		    $category491 = $categories->appendChild($dom->createElement('category'));
		    $category491->setAttribute('id', '70');
		    $category491->setAttribute('parentId', '69');
		    $category491->appendChild($dom->createTextNode('Шланги газоотводные'));

		$category4a1 = $categories->appendChild($dom->createElement('category'));
	    $category4a1->setAttribute('id', '71');
	    $category4a1->setAttribute('parentId', '64');
	    $category4a1->appendChild($dom->createTextNode('Установка для мойки деталей'));

	$category5 = $categories->appendChild($dom->createElement('category'));
    $category5->setAttribute('id', '72');
    $category5->appendChild($dom->createTextNode('Сварочное оборудование'));

	    $category51 = $categories->appendChild($dom->createElement('category'));
	    $category51->setAttribute('id', '73');
	    $category51->setAttribute('parentId', '72');
	    $category51->appendChild($dom->createTextNode('Аксессуары для точечной сварки'));

	    $category52 = $categories->appendChild($dom->createElement('category'));
	    $category52->setAttribute('id', '74');
	    $category52->setAttribute('parentId', '72');
	    $category52->appendChild($dom->createTextNode('Аппараты точечной сварки (споттеры)'));

	$category6 = $categories->appendChild($dom->createElement('category'));
    $category6->setAttribute('id', '75');
    $category6->appendChild($dom->createTextNode('Диагностическое оборудование'));

    	$category61 = $categories->appendChild($dom->createElement('category'));
	    $category61->setAttribute('id', '76');
	    $category61->setAttribute('parentId', '75');
	    $category61->appendChild($dom->createTextNode('Заправка кондиционеров автомобиля'));

	    $category62 = $categories->appendChild($dom->createElement('category'));
	    $category62->setAttribute('id', '77');
	    $category62->setAttribute('parentId', '75');
	    $category62->appendChild($dom->createTextNode('Приборы регулировки фар'));

	    $category63 = $categories->appendChild($dom->createElement('category'));
	    $category63->setAttribute('id', '78');
	    $category63->setAttribute('parentId', '75');
	    $category63->appendChild($dom->createTextNode('Установки для диагностики и очистки форсунок'));

	    $category64 = $categories->appendChild($dom->createElement('category'));
	    $category64->setAttribute('id', '789');
	    $category64->setAttribute('parentId', '75');
	    $category64->appendChild($dom->createTextNode('Установки для промывки топливных систем'));

	$category7 = $categories->appendChild($dom->createElement('category'));
    $category7->setAttribute('id', '79');
    $category7->appendChild($dom->createTextNode('Малая диагностика'));

    	$category71 = $categories->appendChild($dom->createElement('category'));
	    $category71->setAttribute('id', '80');
	    $category71->setAttribute('parentId', '79');
	    $category71->appendChild($dom->createTextNode('Вакууметры'));

	    $category72 = $categories->appendChild($dom->createElement('category'));
	    $category72->setAttribute('id', '81');
	    $category72->setAttribute('parentId', '79');
	    $category72->appendChild($dom->createTextNode('Диагностические наборы'));

		$category73 = $categories->appendChild($dom->createElement('category'));
	    $category73->setAttribute('id', '82');
	    $category73->setAttribute('parentId', '79');
	    $category73->appendChild($dom->createTextNode('Компрессометры'));

	    $category74 = $categories->appendChild($dom->createElement('category'));
	    $category74->setAttribute('id', '83');
	    $category74->setAttribute('parentId', '79');
	    $category74->appendChild($dom->createTextNode('Манометры'));

	    $category75 = $categories->appendChild($dom->createElement('category'));
	    $category75->setAttribute('id', '84');
	    $category75->setAttribute('parentId', '79');
	    $category75->appendChild($dom->createTextNode('Тестеры'));

	$category8 = $categories->appendChild($dom->createElement('category'));
    $category8->setAttribute('id', '85');
    $category8->appendChild($dom->createTextNode('Ремонт автомобильных кондиционеров'));

    $category9 = $categories->appendChild($dom->createElement('category'));
    $category9->setAttribute('id', '86');
    $category9->appendChild($dom->createTextNode('Металическая мебель'));

    	$category91 = $categories->appendChild($dom->createElement('category'));
	    $category91->setAttribute('id', '87');
	    $category91->setAttribute('parentId', '86');
	    $category91->appendChild($dom->createTextNode('Инструментальные верстаки'));

	    $category75 = $categories->appendChild($dom->createElement('category'));
	    $category75->setAttribute('id', '88');
	    $category75->setAttribute('parentId', '86');
	    $category75->appendChild($dom->createTextNode('Слесарные верстаки'));

	    $category75 = $categories->appendChild($dom->createElement('category'));
	    $category75->setAttribute('id', '89');
	    $category75->setAttribute('parentId', '86');
	    $category75->appendChild($dom->createTextNode('Специализированная мебель'));

	    $category75 = $categories->appendChild($dom->createElement('category'));
	    $category75->setAttribute('id', '90');
	    $category75->setAttribute('parentId', '86');
	    $category75->appendChild($dom->createTextNode('Хранение инструмента'));

	$categorya1 = $categories->appendChild($dom->createElement('category'));
    $categorya1->setAttribute('id', '91');
    $categorya1->appendChild($dom->createTextNode('Специнструмент'));

    	$categorya11 = $categories->appendChild($dom->createElement('category'));
	    $categorya11->setAttribute('id', '92');
	    $categorya11->setAttribute('parentId', '91');
	    $categorya11->appendChild($dom->createTextNode('Аппараты сварки пластика'));

	    $categorya12 = $categories->appendChild($dom->createElement('category'));
	    $categorya12->setAttribute('id', '93');
	    $categorya12->setAttribute('parentId', '91');
	    $categorya12->appendChild($dom->createTextNode('Кузовной инструмент'));

	    $categorya13 = $categories->appendChild($dom->createElement('category'));
	    $categorya13->setAttribute('id', '94');
	    $categorya13->setAttribute('parentId', '91');
	    $categorya13->appendChild($dom->createTextNode('Специнструмент для двигателя'));

	    $categorya14 = $categories->appendChild($dom->createElement('category'));
	    $categorya14->setAttribute('id', '95');
	    $categorya14->setAttribute('parentId', '91');
	    $categorya14->appendChild($dom->createTextNode('Специнструмент для подвески'));

		    $categorya141 = $categories->appendChild($dom->createElement('category'));
		    $categorya141->setAttribute('id', '96');
		    $categorya141->setAttribute('parentId', '95');
		    $categorya141->appendChild($dom->createTextNode('Обратные молотки'));

		    $categorya142 = $categories->appendChild($dom->createElement('category'));
		    $categorya142->setAttribute('id', '97');
		    $categorya142->setAttribute('parentId', '95');
		    $categorya142->appendChild($dom->createTextNode('Съемники для ступицы'));

		    $categorya143 = $categories->appendChild($dom->createElement('category'));
		    $categorya143->setAttribute('id', '98');
		    $categorya143->setAttribute('parentId', '95');
		    $categorya143->appendChild($dom->createTextNode('Съемники для тормозной системы'));

		    $categorya144 = $categories->appendChild($dom->createElement('category'));
		    $categorya144->setAttribute('id', '99');
		    $categorya144->setAttribute('parentId', '95');
		    $categorya144->appendChild($dom->createTextNode('Съемники для ШРУС'));

		    $categorya145 = $categories->appendChild($dom->createElement('category'));
		    $categorya145->setAttribute('id', '100');
		    $categorya145->setAttribute('parentId', '95');
		    $categorya145->appendChild($dom->createTextNode('Съемники пружин'));

		    $categorya146 = $categories->appendChild($dom->createElement('category'));
		    $categorya146->setAttribute('id', '101');
		    $categorya146->setAttribute('parentId', '95');
		    $categorya146->appendChild($dom->createTextNode('Съемники сайлентблоков'));

		    $categorya147 = $categories->appendChild($dom->createElement('category'));
		    $categorya147->setAttribute('id', '102');
		    $categorya147->setAttribute('parentId', '95');
		    $categorya147->appendChild($dom->createTextNode('Съемники шаровых опор, рулевых тяг'));

		$categorya15 = $categories->appendChild($dom->createElement('category'));
	    $categorya15->setAttribute('id', '103');
	    $categorya15->setAttribute('parentId', '91');
	    $categorya15->appendChild($dom->createTextNode('Кузовной инструмент и оборудование'));

		    $categorya151 = $categories->appendChild($dom->createElement('category'));
		    $categorya151->setAttribute('id', '104');
		    $categorya151->setAttribute('parentId', '103');
		    $categorya151->appendChild($dom->createTextNode('Безынерционные киянки'));

		    $categorya152 = $categories->appendChild($dom->createElement('category'));
		    $categorya152->setAttribute('id', '105');
		    $categorya152->setAttribute('parentId', '103');
		    $categorya152->appendChild($dom->createTextNode('Вытяжные зажимы и цепи'));

		    $categorya153 = $categories->appendChild($dom->createElement('category'));
		    $categorya153->setAttribute('id', '106');
		    $categorya153->setAttribute('parentId', '103');
		    $categorya153->appendChild($dom->createTextNode('Дыроколы'));

		    $categorya154 = $categories->appendChild($dom->createElement('category'));
		    $categorya154->setAttribute('id', '107');
		    $categorya154->setAttribute('parentId', '103');
		    $categorya154->appendChild($dom->createTextNode('Киянки со сменными насадками'));

		    $categorya155 = $categories->appendChild($dom->createElement('category'));
		    $categorya155->setAttribute('id', '108');
		    $categorya155->setAttribute('parentId', '103');
		    $categorya155->appendChild($dom->createTextNode('Монтировки'));

		    $categorya156 = $categories->appendChild($dom->createElement('category'));
		    $categorya156->setAttribute('id', '109');
		    $categorya156->setAttribute('parentId', '103');
		    $categorya156->appendChild($dom->createTextNode('Наборы гидравлического инструмента'));

		    $categorya157 = $categories->appendChild($dom->createElement('category'));
		    $categorya157->setAttribute('id', '110');
		    $categorya157->setAttribute('parentId', '103');
		    $categorya157->appendChild($dom->createTextNode('Наборы рихтовочных молотков'));

		    $categorya158 = $categories->appendChild($dom->createElement('category'));
		    $categorya158->setAttribute('id', '111');
		    $categorya158->setAttribute('parentId', '103');
		    $categorya158->appendChild($dom->createTextNode('Насосы, цилиндры, шланги'));

		    $categorya159 = $categories->appendChild($dom->createElement('category'));
		    $categorya159->setAttribute('id', '112');
		    $categorya159->setAttribute('parentId', '103');
		    $categorya159->appendChild($dom->createTextNode('Обратные молотки'));

		    $categorya159a1 = $categories->appendChild($dom->createElement('category'));
		    $categorya159a1->setAttribute('id', '113');
		    $categorya159a1->setAttribute('parentId', '103');
		    $categorya159a1->appendChild($dom->createTextNode('Поддержки, лопатки и наковальни'));

		    $categorya159a2 = $categories->appendChild($dom->createElement('category'));
		    $categorya159a2->setAttribute('id', '114');
		    $categorya159a2->setAttribute('parentId', '103');
		    $categorya159a2->appendChild($dom->createTextNode('Рихтовочные молотки'));

		    $categorya159a3 = $categories->appendChild($dom->createElement('category'));
		    $categorya159a3->setAttribute('id', '115');
		    $categorya159a3->setAttribute('parentId', '103');
		    $categorya159a3->appendChild($dom->createTextNode('Рихтовочные рубанки и шлифблоки'));

		    $categorya159a4 = $categories->appendChild($dom->createElement('category'));
		    $categorya159a4->setAttribute('id', '116');
		    $categorya159a4->setAttribute('parentId', '103');
		    $categorya159a4->appendChild($dom->createTextNode('Сменные насадки для киянок'));

		    $categorya159a5 = $categories->appendChild($dom->createElement('category'));
		    $categorya159a5->setAttribute('id', '117');
		    $categorya159a5->setAttribute('parentId', '103');
		    $categorya159a5->appendChild($dom->createTextNode('Стенды для ремонта кузова (стапели)'));

			    $categorya159a51 = $categories->appendChild($dom->createElement('category'));
			    $categorya159a51->setAttribute('id', '118');
			    $categorya159a51->setAttribute('parentId', '117');
			    $categorya159a51->appendChild($dom->createTextNode('Аксессуары для стапелей'));

			$categorya159a6 = $categories->appendChild($dom->createElement('category'));
		    $categorya159a6->setAttribute('id', '119');
		    $categorya159a6->setAttribute('parentId', '103');
		    $categorya159a6->appendChild($dom->createTextNode('Ударные поворотные отвертки'));

		    $categorya159a7 = $categories->appendChild($dom->createElement('category'));
		    $categorya159a7->setAttribute('id', '120');
		    $categorya159a7->setAttribute('parentId', '103');
		    $categorya159a7->appendChild($dom->createTextNode('Шпатели'));

		$categorya16 = $categories->appendChild($dom->createElement('category'));
	    $categorya16->setAttribute('id', '121');
	    $categorya16->setAttribute('parentId', '91');
	    $categorya16->appendChild($dom->createTextNode('Специнструмент для узлов и агрегатов'));

		    $categorya16a1 = $categories->appendChild($dom->createElement('category'));
		    $categorya16a1->setAttribute('id', '122');
		    $categorya16a1->setAttribute('parentId', '121');
		    $categorya16a1->appendChild($dom->createTextNode('Гидравлические съемники'));

		    $categorya16a2 = $categories->appendChild($dom->createElement('category'));
		    $categorya16a2->setAttribute('id', '123');
		    $categorya16a2->setAttribute('parentId', '121');
		    $categorya16a2->appendChild($dom->createTextNode('Специнструмент для замены колес'));

		    $categorya16a3 = $categories->appendChild($dom->createElement('category'));
		    $categorya16a3->setAttribute('id', '124');
		    $categorya16a3->setAttribute('parentId', '121');
		    $categorya16a3->appendChild($dom->createTextNode('Специнструмент для замены стекол'));

		    $categorya16a4 = $categories->appendChild($dom->createElement('category'));
		    $categorya16a4->setAttribute('id', '125');
		    $categorya16a4->setAttribute('parentId', '121');
		    $categorya16a4->appendChild($dom->createTextNode('Специнструмент для извлечения обломков'));

		    $categorya16a5 = $categories->appendChild($dom->createElement('category'));
		    $categorya16a5->setAttribute('id', '126');
		    $categorya16a5->setAttribute('parentId', '121');
		    $categorya16a5->appendChild($dom->createTextNode('Специнструмент для кондиционеров'));

		    $categorya16a6 = $categories->appendChild($dom->createElement('category'));
		    $categorya16a6->setAttribute('id', '127');
		    $categorya16a6->setAttribute('parentId', '121');
		    $categorya16a6->appendChild($dom->createTextNode('Специнструмент для ремонта салона'));

		    $categorya16a7 = $categories->appendChild($dom->createElement('category'));
		    $categorya16a7->setAttribute('id', '128');
		    $categorya16a7->setAttribute('parentId', '121');
		    $categorya16a7->appendChild($dom->createTextNode('Специнструмент для системы охлаждения'));

		    $categorya16a8 = $categories->appendChild($dom->createElement('category'));
		    $categorya16a8->setAttribute('id', '129');
		    $categorya16a8->setAttribute('parentId', '121');
		    $categorya16a8->appendChild($dom->createTextNode('Съемники подшипников'));

		    $categorya16a9 = $categories->appendChild($dom->createElement('category'));
		    $categorya16a9->setAttribute('id', '130');
		    $categorya16a9->setAttribute('parentId', '121');
		    $categorya16a9->appendChild($dom->createTextNode('Усилители крутящего момента'));

		$categorya17 = $categories->appendChild($dom->createElement('category'));
	    $categorya17->setAttribute('id', '131');
	    $categorya17->setAttribute('parentId', '91');
	    $categorya17->appendChild($dom->createTextNode('Специальные торцевые головки'));

	    	$categorya17a1 = $categories->appendChild($dom->createElement('category'));
		    $categorya17a1->setAttribute('id', '132');
		    $categorya17a1->setAttribute('parentId', '131');
		    $categorya17a1->appendChild($dom->createTextNode('Восьмигранные торцевые головки'));

		    $categorya17a2 = $categories->appendChild($dom->createElement('category'));
		    $categorya17a2->setAttribute('id', '133');
		    $categorya17a2->setAttribute('parentId', '131');
		    $categorya17a2->appendChild($dom->createTextNode('Торцевые головки RIBE'));

		    $categorya17a3 = $categories->appendChild($dom->createElement('category'));
		    $categorya17a3->setAttribute('id', '134');
		    $categorya17a3->setAttribute('parentId', '131');
		    $categorya17a3->appendChild($dom->createTextNode('Четырехгранные футорочные головки'));

		$categorya18 = $categories->appendChild($dom->createElement('category'));
	    $categorya18->setAttribute('id', '135');
	    $categorya18->setAttribute('parentId', '91');
	    $categorya18->appendChild($dom->createTextNode('Инструмент для обработки труб'));

	    $categorya19 = $categories->appendChild($dom->createElement('category'));
	    $categorya19->setAttribute('id', '136');
	    $categorya19->setAttribute('parentId', '91');
	    $categorya19->appendChild($dom->createTextNode('Вспомогательный инструмент'));

	$categorya2 = $categories->appendChild($dom->createElement('category'));
    $categorya2->setAttribute('id', '137');
    $categorya2->appendChild($dom->createTextNode('Ручной инструмент'));

    	$categorya21 = $categories->appendChild($dom->createElement('category'));
	    $categorya21->setAttribute('id', '138');
	    $categorya21->setAttribute('parentId', '137');
	    $categorya21->appendChild($dom->createTextNode('Инструментальные тележки, ложементы'));

		    $categorya211 = $categories->appendChild($dom->createElement('category'));
		    $categorya211->setAttribute('id', '139');
		    $categorya211->setAttribute('parentId', '138');
		    $categorya211->appendChild($dom->createTextNode('Инструмент в ложементах'));

		    $categorya212 = $categories->appendChild($dom->createElement('category'));
		    $categorya212->setAttribute('id', '140');
		    $categorya212->setAttribute('parentId', '138');
		    $categorya212->appendChild($dom->createTextNode('Инструментальные тележки'));

		    $categorya213 = $categories->appendChild($dom->createElement('category'));
		    $categorya213->setAttribute('id', '141');
		    $categorya213->setAttribute('parentId', '138');
		    $categorya213->appendChild($dom->createTextNode('Комплекты из ложементов'));

		$categorya22 = $categories->appendChild($dom->createElement('category'));
	    $categorya22->setAttribute('id', '142');
	    $categorya22->setAttribute('parentId', '137');
	    $categorya22->appendChild($dom->createTextNode('Универсальные наборы инструментов'));

	    $categorya23 = $categories->appendChild($dom->createElement('category'));
	    $categorya23->setAttribute('id', '143');
	    $categorya23->setAttribute('parentId', '137');
	    $categorya23->appendChild($dom->createTextNode('Динамометрические ключи'));

	    $categorya24 = $categories->appendChild($dom->createElement('category'));
	    $categorya24->setAttribute('id', '144');
	    $categorya24->setAttribute('parentId', '137');
	    $categorya24->appendChild($dom->createTextNode('Торцевые головки'));

		    $categorya241 = $categories->appendChild($dom->createElement('category'));
		    $categorya241->setAttribute('id', '145');
		    $categorya241->setAttribute('parentId', '144');
		    $categorya241->appendChild($dom->createTextNode('Глубокие торцевые головки'));

		    $categorya242 = $categories->appendChild($dom->createElement('category'));
		    $categorya242->setAttribute('id', '146');
		    $categorya242->setAttribute('parentId', '144');
		    $categorya242->appendChild($dom->createTextNode('Головки для колесных болтов'));

		    $categorya243 = $categories->appendChild($dom->createElement('category'));
		    $categorya243->setAttribute('id', '147');
		    $categorya243->setAttribute('parentId', '144');
		    $categorya243->appendChild($dom->createTextNode('Головки для шуруповерта'));

		    $categorya244 = $categories->appendChild($dom->createElement('category'));
		    $categorya244->setAttribute('id', '148');
		    $categorya244->setAttribute('parentId', '144');
		    $categorya244->appendChild($dom->createTextNode('Наборы торцевых головок'));

		    $categorya245 = $categories->appendChild($dom->createElement('category'));
		    $categorya245->setAttribute('id', '149');
		    $categorya245->setAttribute('parentId', '144');
		    $categorya245->appendChild($dom->createTextNode('Свечные головки'));

		    $categorya246 = $categories->appendChild($dom->createElement('category'));
		    $categorya246->setAttribute('id', '150');
		    $categorya246->setAttribute('parentId', '144');
		    $categorya246->appendChild($dom->createTextNode('Стандартные торцевые головки'));

		    $categorya247 = $categories->appendChild($dom->createElement('category'));
		    $categorya247->setAttribute('id', '151');
		    $categorya247->setAttribute('parentId', '144');
		    $categorya247->appendChild($dom->createTextNode('Торцевые головки TORX Е-стандарт'));

		    $categorya248 = $categories->appendChild($dom->createElement('category'));
		    $categorya248->setAttribute('id', '152');
		    $categorya248->setAttribute('parentId', '144');
		    $categorya248->appendChild($dom->createTextNode('Ударные глубокие торцевые головки'));

		    $categorya249 = $categories->appendChild($dom->createElement('category'));
		    $categorya249->setAttribute('id', '153');
		    $categorya249->setAttribute('parentId', '144');
		    $categorya249->appendChild($dom->createTextNode('Ударные торцевые головки'));

		$categorya25 = $categories->appendChild($dom->createElement('category'));
	    $categorya25->setAttribute('id', '154');
	    $categorya25->setAttribute('parentId', '137');
	    $categorya25->appendChild($dom->createTextNode('Принадлежности к торцевым головкам'));

	    	$categorya251 = $categories->appendChild($dom->createElement('category'));
		    $categorya251->setAttribute('id', '155');
		    $categorya251->setAttribute('parentId', '154');
		    $categorya251->appendChild($dom->createTextNode('Воротки'));

		    $categorya252 = $categories->appendChild($dom->createElement('category'));
		    $categorya252->setAttribute('id', '156');
		    $categorya252->setAttribute('parentId', '154');
		    $categorya252->appendChild($dom->createTextNode('Держатели головок'));

		    $categorya253 = $categories->appendChild($dom->createElement('category'));
		    $categorya253->setAttribute('id', '157');
		    $categorya253->setAttribute('parentId', '154');
		    $categorya253->appendChild($dom->createTextNode('Карданы'));

		    $categorya254 = $categories->appendChild($dom->createElement('category'));
		    $categorya254->setAttribute('id', '158');
		    $categorya254->setAttribute('parentId', '154');
		    $categorya254->appendChild($dom->createTextNode('Переходники'));

		    $categorya255 = $categories->appendChild($dom->createElement('category'));
		    $categorya255->setAttribute('id', '159');
		    $categorya255->setAttribute('parentId', '154');
		    $categorya255->appendChild($dom->createTextNode('Трещотки'));

		    $categorya256 = $categories->appendChild($dom->createElement('category'));
		    $categorya256->setAttribute('id', '160');
		    $categorya256->setAttribute('parentId', '154');
		    $categorya256->appendChild($dom->createTextNode('Удлинители'));

		    $categorya257 = $categories->appendChild($dom->createElement('category'));
		    $categorya257->setAttribute('id', '161');
		    $categorya257->setAttribute('parentId', '154');
		    $categorya257->appendChild($dom->createTextNode('Удлинители гибкие'));

		$categorya26 = $categories->appendChild($dom->createElement('category'));
	    $categorya26->setAttribute('id', '162');
	    $categorya26->setAttribute('parentId', '137');
	    $categorya26->appendChild($dom->createTextNode('Торцевые насадки, вставки (биты)'));

	    	$categorya261 = $categories->appendChild($dom->createElement('category'));
		    $categorya261->setAttribute('id', '163');
		    $categorya261->setAttribute('parentId', '162');
		    $categorya261->appendChild($dom->createTextNode('Воротки, трещотки и адаптеры'));

		    $categorya262 = $categories->appendChild($dom->createElement('category'));
		    $categorya262->setAttribute('id', '164');
		    $categorya262->setAttribute('parentId', '162');
		    $categorya262->appendChild($dom->createTextNode('Наборы торцевых насадок и бит'));

		    $categorya263 = $categories->appendChild($dom->createElement('category'));
		    $categorya263->setAttribute('id', '165');
		    $categorya263->setAttribute('parentId', '162');
		    $categorya263->appendChild($dom->createTextNode('Торцевые вставки (биты)'));

		    $categorya264 = $categories->appendChild($dom->createElement('category'));
		    $categorya264->setAttribute('id', '16511');
		    $categorya264->setAttribute('parentId', '162');
		    $categorya264->appendChild($dom->createTextNode('Торцевые насадки'));

		$categorya27 = $categories->appendChild($dom->createElement('category'));
	    $categorya27->setAttribute('id', '166');
	    $categorya27->setAttribute('parentId', '137');
	    $categorya27->appendChild($dom->createTextNode('Слесарные ключи'));

	    	$categorya271 = $categories->appendChild($dom->createElement('category'));
		    $categorya271->setAttribute('id', '167');
		    $categorya271->setAttribute('parentId', '166');
		    $categorya271->appendChild($dom->createTextNode('Комбинированные изогнутые ключи'));

		    $categorya272 = $categories->appendChild($dom->createElement('category'));
		    $categorya272->setAttribute('id', '168');
		    $categorya272->setAttribute('parentId', '166');
		    $categorya272->appendChild($dom->createTextNode('Комбинированные ключи'));

		    $categorya273 = $categories->appendChild($dom->createElement('category'));
		    $categorya273->setAttribute('id', '169');
		    $categorya273->setAttribute('parentId', '166');
		    $categorya273->appendChild($dom->createTextNode('Комбинированные шарнирные ключи'));

		    $categorya274 = $categories->appendChild($dom->createElement('category'));
		    $categorya274->setAttribute('id', '170');
		    $categorya274->setAttribute('parentId', '166');
		    $categorya274->appendChild($dom->createTextNode('Наборы ключей'));

		    $categorya275 = $categories->appendChild($dom->createElement('category'));
		    $categorya275->setAttribute('id', '171');
		    $categorya275->setAttribute('parentId', '166');
		    $categorya275->appendChild($dom->createTextNode('Накидные ключи'));

		    $categorya276 = $categories->appendChild($dom->createElement('category'));
		    $categorya276->setAttribute('id', '172');
		    $categorya276->setAttribute('parentId', '166');
		    $categorya276->appendChild($dom->createTextNode('Накидные ключи Е-профиль (звезда)'));

		    $categorya277 = $categories->appendChild($dom->createElement('category'));
		    $categorya277->setAttribute('id', '173');
		    $categorya277->setAttribute('parentId', '166');
		    $categorya277->appendChild($dom->createTextNode('Накидные прямые удлинённые ключи'));

		    $categorya278 = $categories->appendChild($dom->createElement('category'));
		    $categorya278->setAttribute('id', '174');
		    $categorya278->setAttribute('parentId', '166');
		    $categorya278->appendChild($dom->createTextNode('Накидные ударные ключи'));

		    $categorya279 = $categories->appendChild($dom->createElement('category'));
		    $categorya279->setAttribute('id', '175');
		    $categorya279->setAttribute('parentId', '166');
		    $categorya279->appendChild($dom->createTextNode('Разводные ключи'));

		    $categorya27a1 = $categories->appendChild($dom->createElement('category'));
		    $categorya27a1->setAttribute('id', '176');
		    $categorya27a1->setAttribute('parentId', '166');
		    $categorya27a1->appendChild($dom->createTextNode('Разрезные ключи'));

		    $categorya27a2 = $categories->appendChild($dom->createElement('category'));
		    $categorya27a2->setAttribute('id', '177');
		    $categorya27a2->setAttribute('parentId', '166');
		    $categorya27a2->appendChild($dom->createTextNode('Рожковые ключи'));

		    $categorya27a3 = $categories->appendChild($dom->createElement('category'));
		    $categorya27a3->setAttribute('id', '178');
		    $categorya27a3->setAttribute('parentId', '166');
		    $categorya27a3->appendChild($dom->createTextNode('Рожковые ударные ключи'));

		    $categorya27a4 = $categories->appendChild($dom->createElement('category'));
		    $categorya27a4->setAttribute('id', '179');
		    $categorya27a4->setAttribute('parentId', '166');
		    $categorya27a4->appendChild($dom->createTextNode('Торцевые L-образные ключи'));

		    $categorya27a5 = $categories->appendChild($dom->createElement('category'));
		    $categorya27a5->setAttribute('id', '180');
		    $categorya27a5->setAttribute('parentId', '166');
		    $categorya27a5->appendChild($dom->createTextNode('Торцевые Г-образные ключи'));

		    $categorya27a6 = $categories->appendChild($dom->createElement('category'));
		    $categorya27a6->setAttribute('id', '181');
		    $categorya27a6->setAttribute('parentId', '166');
		    $categorya27a6->appendChild($dom->createTextNode('Шарнирные торцевые ключи (колокольчики)'));

		$categorya28 = $categories->appendChild($dom->createElement('category'));
	    $categorya28->setAttribute('id', '182');
	    $categorya28->setAttribute('parentId', '137');
	    $categorya28->appendChild($dom->createTextNode('Трещоточные ключи'));

	    $categorya29 = $categories->appendChild($dom->createElement('category'));
	    $categorya29->setAttribute('id', '183');
	    $categorya29->setAttribute('parentId', '137');
	    $categorya29->appendChild($dom->createTextNode('Специальные ключи'));

	    	$categorya291 = $categories->appendChild($dom->createElement('category'));
		    $categorya291->setAttribute('id', '184');
		    $categorya291->setAttribute('parentId', '183');
		    $categorya291->appendChild($dom->createTextNode('Балонные ключи'));

		    $categorya292 = $categories->appendChild($dom->createElement('category'));
		    $categorya292->setAttribute('id', '185');
		    $categorya292->setAttribute('parentId', '183');
		    $categorya292->appendChild($dom->createTextNode('Радиусные ключи'));

		    $categorya293 = $categories->appendChild($dom->createElement('category'));
		    $categorya293->setAttribute('id', '186');
		    $categorya293->setAttribute('parentId', '183');
		    $categorya293->appendChild($dom->createTextNode('Свечные ключи'));

		    $categorya294 = $categories->appendChild($dom->createElement('category'));
		    $categorya294->setAttribute('id', '187');
		    $categorya294->setAttribute('parentId', '183');
		    $categorya294->appendChild($dom->createTextNode('Стартерные ключи'));

		    $categorya295 = $categories->appendChild($dom->createElement('category'));
		    $categorya295->setAttribute('id', '188');
		    $categorya295->setAttribute('parentId', '183');
		    $categorya295->appendChild($dom->createTextNode('Торцевые Т-образные ключи'));

		    $categorya296 = $categories->appendChild($dom->createElement('category'));
		    $categorya296->setAttribute('id', '189');
		    $categorya296->setAttribute('parentId', '183');
		    $categorya296->appendChild($dom->createTextNode('Трубные и газовые ключи'));

		$categorya2a1 = $categories->appendChild($dom->createElement('category'));
	    $categorya2a1->setAttribute('id', '190');
	    $categorya2a1->setAttribute('parentId', '137');
	    $categorya2a1->appendChild($dom->createTextNode('Отвертки и шестигранники'));

	    	$categorya2a11 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a11->setAttribute('id', '1910');
		    $categorya2a11->setAttribute('parentId', '190');
		    $categorya2a11->appendChild($dom->createTextNode('Диэлектрические отвертки'));

		    $categorya2a12 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a12->setAttribute('id', '1920');
		    $categorya2a12->setAttribute('parentId', '190');
		    $categorya2a12->appendChild($dom->createTextNode('Наборы отверток и шестигранников'));

		    $categorya2a13 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a13->setAttribute('id', '1930');
		    $categorya2a13->setAttribute('parentId', '190');
		    $categorya2a13->appendChild($dom->createTextNode('Отвертки со сменными вставками'));

		    $categorya2a14 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a14->setAttribute('id', '1940');
		    $categorya2a14->setAttribute('parentId', '190');
		    $categorya2a14->appendChild($dom->createTextNode('Прецизионные отвертки'));

		    $categorya2a15 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a15->setAttribute('id', '1950');
		    $categorya2a15->setAttribute('parentId', '190');
		    $categorya2a15->appendChild($dom->createTextNode('Стандартные отвертки'));

		    $categorya2a16 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a16->setAttribute('id', '1960');
		    $categorya2a16->setAttribute('parentId', '190');
		    $categorya2a16->appendChild($dom->createTextNode('Торцевые отвертки'));

		    $categorya2a17 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a17->setAttribute('id', '1970');
		    $categorya2a17->setAttribute('parentId', '190');
		    $categorya2a17->appendChild($dom->createTextNode('Усиленные отвертки'));

		    $categorya2a18 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a18->setAttribute('id', '1980');
		    $categorya2a18->setAttribute('parentId', '190');
		    $categorya2a18->appendChild($dom->createTextNode('Шестигранники Г-образные'));

		$categorya2a2 = $categories->appendChild($dom->createElement('category'));
	    $categorya2a2->setAttribute('id', '191');
	    $categorya2a2->setAttribute('parentId', '137');
	    $categorya2a2->appendChild($dom->createTextNode('Шарнирно-губцевый инструмент'));

	    	$categorya2a21 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a21->setAttribute('id', '192');
		    $categorya2a21->setAttribute('parentId', '191');
		    $categorya2a21->appendChild($dom->createTextNode('Бокорезы'));

		    $categorya2a22 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a22->setAttribute('id', '193');
		    $categorya2a22->setAttribute('parentId', '191');
		    $categorya2a22->appendChild($dom->createTextNode('Диэлектрический инструмент'));

		    $categorya2a23 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a23->setAttribute('id', '194');
		    $categorya2a23->setAttribute('parentId', '191');
		    $categorya2a23->appendChild($dom->createTextNode('Зажимы с фиксатором'));

		    $categorya2a24 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a24->setAttribute('id', '195');
		    $categorya2a24->setAttribute('parentId', '191');
		    $categorya2a24->appendChild($dom->createTextNode('Круглогубцы'));

		    $categorya2a25 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a25->setAttribute('id', '196');
		    $categorya2a25->setAttribute('parentId', '191');
		    $categorya2a25->appendChild($dom->createTextNode('Кусачки'));

		    $categorya2a26 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a26->setAttribute('id', '197');
		    $categorya2a26->setAttribute('parentId', '191');
		    $categorya2a26->appendChild($dom->createTextNode('Наборы шарнирно-губцевого инструмента'));

		    $categorya2a27 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a27->setAttribute('id', '198');
		    $categorya2a27->setAttribute('parentId', '191');
		    $categorya2a27->appendChild($dom->createTextNode('Пассатижи очистки изоляции'));

		    $categorya2a28 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a28->setAttribute('id', '199');
		    $categorya2a28->setAttribute('parentId', '191');
		    $categorya2a28->appendChild($dom->createTextNode('Переставные клещи'));

		    $categorya2a29 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a29->setAttribute('id', '1961');
		    $categorya2a29->setAttribute('parentId', '191');
		    $categorya2a29->appendChild($dom->createTextNode('Плоскогубцы и пассатижи'));

		    $categorya2a2a1 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a2a1->setAttribute('id', '1971');
		    $categorya2a2a1->setAttribute('parentId', '191');
		    $categorya2a2a1->appendChild($dom->createTextNode('Съемники для cтопорных колец'));

		    $categorya2a2a2 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a2a2->setAttribute('id', '1981');
		    $categorya2a2a2->setAttribute('parentId', '191');
		    $categorya2a2a2->appendChild($dom->createTextNode('Удлиненные плоскогубцы (утконосы)'));

		    $categorya2a2a3 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a2a3->setAttribute('id', '1991');
		    $categorya2a2a3->setAttribute('parentId', '191');
		    $categorya2a2a3->appendChild($dom->createTextNode('Шиномонтажные клещи'));

		$categorya2a3 = $categories->appendChild($dom->createElement('category'));
	    $categorya2a3->setAttribute('id', '200');
	    $categorya2a3->setAttribute('parentId', '137');
	    $categorya2a3->appendChild($dom->createTextNode('Ударный инструмент'));

	    	$categorya2a31 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a31->setAttribute('id', '201');
		    $categorya2a31->setAttribute('parentId', '200');
		    $categorya2a31->appendChild($dom->createTextNode('Бородки'));

		    $categorya2a32 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a32->setAttribute('id', '202');
		    $categorya2a32->setAttribute('parentId', '200');
		    $categorya2a32->appendChild($dom->createTextNode('Выколотки'));

		    $categorya2a33 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a33->setAttribute('id', '203');
		    $categorya2a33->setAttribute('parentId', '200');
		    $categorya2a33->appendChild($dom->createTextNode('Зубила'));

		    $categorya2a34 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a34->setAttribute('id', '204');
		    $categorya2a34->setAttribute('parentId', '200');
		    $categorya2a34->appendChild($dom->createTextNode('Кернеры'));

		    $categorya2a35 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a35->setAttribute('id', '205');
		    $categorya2a35->setAttribute('parentId', '200');
		    $categorya2a35->appendChild($dom->createTextNode('Клейма'));

		    $categorya2a36 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a36->setAttribute('id', '206');
		    $categorya2a36->setAttribute('parentId', '200');
		    $categorya2a36->appendChild($dom->createTextNode('Молотки и кувалды'));

		$categorya2a4 = $categories->appendChild($dom->createElement('category'));
	    $categorya2a4->setAttribute('id', '207');
	    $categorya2a4->setAttribute('parentId', '137');
	    $categorya2a4->appendChild($dom->createTextNode('Режущий инструмент'));

	    	$categorya2a31 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a31->setAttribute('id', '208');
		    $categorya2a31->setAttribute('parentId', '207');
		    $categorya2a31->appendChild($dom->createTextNode('Болторезы'));

		    $categorya2a32 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a32->setAttribute('id', '209');
		    $categorya2a32->setAttribute('parentId', '207');
		    $categorya2a32->appendChild($dom->createTextNode('Буры по бетону'));

		    $categorya2a33 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a33->setAttribute('id', '210');
		    $categorya2a33->setAttribute('parentId', '207');
		    $categorya2a33->appendChild($dom->createTextNode('Восстановители резьбы'));

		    $categorya2a34 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a34->setAttribute('id', '211');
		    $categorya2a34->setAttribute('parentId', '207');
		    $categorya2a34->appendChild($dom->createTextNode('Метчики'));

		    $categorya2a35 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a35->setAttribute('id', '212');
		    $categorya2a35->setAttribute('parentId', '207');
		    $categorya2a35->appendChild($dom->createTextNode('Набор сверел по металлу'));

		    $categorya2a36 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a36->setAttribute('id', '213');
		    $categorya2a36->setAttribute('parentId', '207');
		    $categorya2a36->appendChild($dom->createTextNode('Наборы для нарезания резьбы'));

		    $categorya2a37 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a37->setAttribute('id', '214');
		    $categorya2a37->setAttribute('parentId', '207');
		    $categorya2a37->appendChild($dom->createTextNode('Надфили'));

		    $categorya2a38 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a38->setAttribute('id', '215');
		    $categorya2a38->setAttribute('parentId', '207');
		    $categorya2a38->appendChild($dom->createTextNode('Напильники'));

		    $categorya2a39 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a39->setAttribute('id', '216');
		    $categorya2a39->setAttribute('parentId', '207');
		    $categorya2a39->appendChild($dom->createTextNode('Ножницы по металлу'));

		    $categorya2a3a1 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a3a1->setAttribute('id', '217');
		    $categorya2a3a1->setAttribute('parentId', '207');
		    $categorya2a3a1->appendChild($dom->createTextNode('Ножовки по дереву'));

		    $categorya2a3a2 = $categories->appendChild($dom->createElement('category'));
		    $categorya2a3a2->setAttribute('id', '218');
		    $categorya2a3a2->setAttribute('parentId', '207');
		    $categorya2a3a2->appendChild($dom->createTextNode('Ножовки по металлу'));

		$categorya2a5 = $categories->appendChild($dom->createElement('category'));
	    $categorya2a5->setAttribute('id', '219');
	    $categorya2a5->setAttribute('parentId', '137');
	    $categorya2a5->appendChild($dom->createTextNode('Разное'));

	    $categorya2a6 = $categories->appendChild($dom->createElement('category'));
	    $categorya2a6->setAttribute('id', '220');
	    $categorya2a6->setAttribute('parentId', '137');
	    $categorya2a6->appendChild($dom->createTextNode('Измерительный инструмент'));

	    $categorya2a7 = $categories->appendChild($dom->createElement('category'));
	    $categorya2a7->setAttribute('id', '221');
	    $categorya2a7->setAttribute('parentId', '137');
	    $categorya2a7->appendChild($dom->createTextNode('Торгово-выставочные стенды'));

	$categorya3 = $categories->appendChild($dom->createElement('category'));
    $categorya3->setAttribute('id', '222');
    $categorya3->appendChild($dom->createTextNode('Пневмоинструмент'));

    	$categorya31 = $categories->appendChild($dom->createElement('category'));
	    $categorya31->setAttribute('id', '223');
	    $categorya31->setAttribute('parentId', '222');
	    $categorya31->appendChild($dom->createTextNode('Наборы пневмоинструмента'));

	    $categorya32 = $categories->appendChild($dom->createElement('category'));
	    $categorya32->setAttribute('id', '224');
	    $categorya32->setAttribute('parentId', '222');
	    $categorya32->appendChild($dom->createTextNode('Обработка металла'));

	    $categorya33 = $categories->appendChild($dom->createElement('category'));
	    $categorya33->setAttribute('id', '225');
	    $categorya33->setAttribute('parentId', '222');
	    $categorya33->appendChild($dom->createTextNode('Окраска кузова'));

	    $categorya34 = $categories->appendChild($dom->createElement('category'));
	    $categorya34->setAttribute('id', '226');
	    $categorya34->setAttribute('parentId', '222');
	    $categorya34->appendChild($dom->createTextNode('Оснастка для пневмоинструмента'));

	    $categorya35 = $categories->appendChild($dom->createElement('category'));
	    $categorya35->setAttribute('id', '227');
	    $categorya35->setAttribute('parentId', '222');
	    $categorya35->appendChild($dom->createTextNode('Пневматические гайковерты, трещотки, дрели'));

	    $categorya36 = $categories->appendChild($dom->createElement('category'));
	    $categorya36->setAttribute('id', '228');
	    $categorya36->setAttribute('parentId', '222');
	    $categorya36->appendChild($dom->createTextNode('Пневматические линии'));

	    	$categorya361 = $categories->appendChild($dom->createElement('category'));
		    $categorya361->setAttribute('id', '229');
		    $categorya361->setAttribute('parentId', '228');
		    $categorya361->appendChild($dom->createTextNode('Автоматические катушки со шлангом'));

		    $categorya362 = $categories->appendChild($dom->createElement('category'));
		    $categorya362->setAttribute('id', '230');
		    $categorya362->setAttribute('parentId', '228');
		    $categorya362->appendChild($dom->createTextNode('Воздушные фильтры, лубрикаторы'));

		    $categorya363 = $categories->appendChild($dom->createElement('category'));
		    $categorya363->setAttribute('id', '231');
		    $categorya363->setAttribute('parentId', '228');
		    $categorya363->appendChild($dom->createTextNode('Пневматические шланги высокого давления'));

		    $categorya364 = $categories->appendChild($dom->createElement('category'));
		    $categorya364->setAttribute('id', '232');
		    $categorya364->setAttribute('parentId', '228');
		    $categorya364->appendChild($dom->createTextNode('Подвесы (балансиры)'));

		    $categorya365 = $categories->appendChild($dom->createElement('category'));
		    $categorya365->setAttribute('id', '233');
		    $categorya365->setAttribute('parentId', '228');
		    $categorya365->appendChild($dom->createTextNode('Штуцера, муфты и разъемы (фитинги)'));

		$categorya38 = $categories->appendChild($dom->createElement('category'));
	    $categorya38->setAttribute('id', '234');
	    $categorya38->setAttribute('parentId', '222');
	    $categorya38->appendChild($dom->createTextNode('Пневматические пистолеты'));

	    $categorya39 = $categories->appendChild($dom->createElement('category'));
	    $categorya39->setAttribute('id', '235');
	    $categorya39->setAttribute('parentId', '222');
	    $categorya39->appendChild($dom->createTextNode('Пневмоинструмент для кузовного ремонта'));

	    $categorya3a1 = $categories->appendChild($dom->createElement('category'));
	    $categorya3a1->setAttribute('id', '236');
	    $categorya3a1->setAttribute('parentId', '222');
	    $categorya3a1->appendChild($dom->createTextNode('Разобрать с мастер-инструмент'));

	    $categorya3a2 = $categories->appendChild($dom->createElement('category'));
	    $categorya3a2->setAttribute('id', '237');
	    $categorya3a2->setAttribute('parentId', '222');
	    $categorya3a2->appendChild($dom->createTextNode('Расходные материалы для пневмоинструмента'));

	    $categorya3a3 = $categories->appendChild($dom->createElement('category'));
	    $categorya3a3->setAttribute('id', '238');
	    $categorya3a3->setAttribute('parentId', '222');
	    $categorya3a3->appendChild($dom->createTextNode('Режущий пневматический инструмент'));

	    $categorya3a4 = $categories->appendChild($dom->createElement('category'));
	    $categorya3a4->setAttribute('id', '239');
	    $categorya3a4->setAttribute('parentId', '222');
	    $categorya3a4->appendChild($dom->createTextNode('Ремкомплекты для пневмоинструмента'));

	    $categorya3a5 = $categories->appendChild($dom->createElement('category'));
	    $categorya3a5->setAttribute('id', '240');
	    $categorya3a5->setAttribute('parentId', '222');
	    $categorya3a5->appendChild($dom->createTextNode('Сборочный пневматический инструмент'));

	    $categorya3a6 = $categories->appendChild($dom->createElement('category'));
	    $categorya3a6->setAttribute('id', '2401');
	    $categorya3a6->setAttribute('parentId', '222');
	    $categorya3a6->appendChild($dom->createTextNode('Аксессуары и модули для пневмоинструмента'));

	$categorya4 = $categories->appendChild($dom->createElement('category'));
    $categorya4->setAttribute('id', '241');
    $categorya4->appendChild($dom->createTextNode('Воздушные компрессоры'));

    	$categorya41 = $categories->appendChild($dom->createElement('category'));
	    $categorya41->setAttribute('id', '242');
	    $categorya41->setAttribute('parentId', '241');
	    $categorya41->appendChild($dom->createTextNode('Компрессоры безмасляные'));

	    $categorya42 = $categories->appendChild($dom->createElement('category'));
	    $categorya42->setAttribute('id', '243');
	    $categorya42->setAttribute('parentId', '241');
	    $categorya42->appendChild($dom->createTextNode('Компрессоры поршневые'));

	    	$categorya421 = $categories->appendChild($dom->createElement('category'));
		    $categorya421->setAttribute('id', '244');
		    $categorya421->setAttribute('parentId', '241');
		    $categorya421->appendChild($dom->createTextNode('Запчасти к поршневым компрессорам'));

	$categorya5 = $categories->appendChild($dom->createElement('category'));
    $categorya5->setAttribute('id', '245');
    $categorya5->appendChild($dom->createTextNode('Электроинструмент'));

    $categorya6 = $categories->appendChild($dom->createElement('category'));
    $categorya6->setAttribute('id', '246');
    $categorya6->appendChild($dom->createTextNode('Электромонтажный инструмент'));

    $categorya7 = $categories->appendChild($dom->createElement('category'));
    $categorya7->setAttribute('id', '247');
    $categorya7->appendChild($dom->createTextNode('Сопутствующие товары'));

    	$categorya71 = $categories->appendChild($dom->createElement('category'));
	    $categorya71->setAttribute('id', '248');
	    $categorya71->setAttribute('parentId', '247');
	    $categorya71->appendChild($dom->createTextNode('Средства защиты'));

	$categorya8 = $categories->appendChild($dom->createElement('category'));
    $categorya8->setAttribute('id', '249');
    $categorya8->appendChild($dom->createTextNode('Автохимия для мойки и автосервиса'));

?>